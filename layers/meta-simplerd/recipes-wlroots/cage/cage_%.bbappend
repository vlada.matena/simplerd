FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://cage@.service"

FILES:${PN} += "${systemd_system_unitdir}/cage@.service"

do_install:append() {
	install -d ${D}${systemd_system_unitdir}
    install -m 0644 ${WORKDIR}/cage@.service ${D}${systemd_system_unitdir}/cage@.service 
}
