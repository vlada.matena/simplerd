FILES:libfreerdp =+ "\
	/home/root/.config \
"

do_install:append() {
	install --directory ${D}/home/root
	ln -s /tmp ${D}/home/root/.config
}
