SUMMARY = "Automounter for removable media"

LICENSE = "MIT"

DEPENDS = "asciidoc-native gettext-native"
RDEPENDS:${PN} = "udisks2 python3-pyyaml pyxdg python3-pygobject python3-docopt python3-logging"

inherit pypi setuptools3 systemd

PYPI_PACKAGE = "udiskie"

SRC_URI += " \
	file://udiskie.service \
	file://udiskie.yaml \
"

SRC_URI[sha256sum] = "1f87ab59cb112915be044fc2ae0b0000a48189af54353793b896ec6c289a0f7c"
LIC_FILES_CHKSUM = "file://COPYING;md5=f7d6dd19680ac1c4995e76442b897458"

do_install:append () {
	install -d ${D}${systemd_system_unitdir}/
	install --mode 644 ${WORKDIR}/udiskie.service ${D}${systemd_system_unitdir}/
	
	install -d ${D}${sysconfdir}
	install --mode 644 ${WORKDIR}/udiskie.yaml ${D}${sysconfdir}/udiskie.yaml
	
	rm ${D}/usr/share/zsh/site-functions/_udiskie
	rm ${D}/usr/share/zsh/site-functions/_udiskie-umount
	rm ${D}/usr/share/zsh/site-functions/_udiskie-mount
	rmdir ${D}/usr/share/zsh/site-functions
	rmdir ${D}/usr/share/zsh
}

SYSTEMD_SERVICE:${PN} = "udiskie.service"
