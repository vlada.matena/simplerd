LICENSE = "MIT"

inherit core-image

IMAGE_INSTALL += "\
	packagegroup-simplerd-core \
	packagegroup-simplerd-apps \
	packagegroup-simplerd-tools \
"

IMAGE_FEATURES += "\
	read-only-rootfs \
	ssh-server-openssh \
	allow-empty-password \
	empty-root-password \
	post-install-logging \
"

IMAGE_LINGUAS = "en-us"

IMAGE_FSTYPES = "wic wic.bmap"
WIC_CREATE_EXTRA_ARGS = "--no-fstab-update"
WKS_FILE_DEPENDS = "e2fsprogs-native bmap-tools-native gptfdisk-native squashfs-tools-native"
WKS_FILE = "rd.wks"

SDKIMAGE_FEATURES = ""

TOOLCHAIN_TARGET_TASK:append = ""

TOOLCHAIN_HOST_TASK:append = ""

DEPENDS += "systemd-systemctl-native coreutils-native"

IMAGE_NAME = "${IMAGE_BASENAME}-${MACHINE}-${DISTRO_VERSION}${IMAGE_VERSION_SUFFIX}"

EXTRA_IMAGEDEPENDS:remove = "qemu-native qemu-helper-native"

# NO_RECOMMENDATIONS = "1"

SYSTEMD_DEFAULT_TARGET = "graphical.target"
