DESCRIPTION = "Basic task to get a device booting"

inherit packagegroup

PACKAGES = "\
	${PN}-core \
	${PN}-apps \
	${PN}-tools \
"

RDEPENDS:${PN}-core = "\
	kernel-image-bzimage-5.15.78-yocto-standard \
	kernel-modules \
	linux-firmware \
	rd-user \
	cage \
	weston \
	sway \
	rd-launch \
	mesa-megadriver \
	kbd-keymaps \
	liberation-fonts \
	random-files \
"

RDEPENDS:${PN}-apps = "\
	freerdp \
	hplip \
	cups \
	ccid \
	pulseaudio \
	ghostscript \
	udiskie \
	foot \
"

RDEPENDS:${PN}-tools = "\
	nano \
	ssh-keys \
	tmux \
	iproute2-ip \
	openssh \
	htop \
"
