SUMMARY = "Random files"
LICENSE = "GPL-3.0-or-later"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-3.0-or-later;md5=1c76c4cc354acaac30ed4d5eefea7245"

inherit systemd

SRC_URI = "\
	file://eth.network \
	file://sway.service \
	file://sway \
"

FILES:${PN} = "\
	/etc/systemd/network/eth.network \
	/etc/systemd/system/sway.service \
	/etc/pam.d/sway \
"

do_install() {
	install -d ${D}/etc/systemd/network
	install -m 0755 ${WORKDIR}/eth.network ${D}/etc/systemd/network/eth.network

	install -d ${D}/etc/systemd/system
	install -m 755 ${WORKDIR}/sway.service ${D}/etc/systemd/system/sway.service

	install -d ${D}/etc/pam.d/
	install -m 755 ${WORKDIR}/sway ${D}/etc/pam.d/sway
}

SYSTEMD_SERVICE:${PN} = "sway.service"
