SUMMARY = "Management ssh keys"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-3.0-or-later;md5=1c76c4cc354acaac30ed4d5eefea7245"

SRC_URI = "\
	file://authorized_keys \
"

FILES:${PN} = "\
	/home/root/.ssh/authorized_keys \
"
do_install() {
	install --directory --group root --owner root --mode 0700 ${D}/home/root/.ssh
	install --group root --owner root --mode 0600 ${WORKDIR}/authorized_keys ${D}/home/root/.ssh/authorized_keys
} 
