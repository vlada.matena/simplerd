SUMMARY = "Remote desktop launch scripts"
LICENSE = "GPL-3.0-or-later"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-3.0-or-later;md5=1c76c4cc354acaac30ed4d5eefea7245"

inherit systemd

SRC_URI = "\
	file://rd-launch.sh \
	file://rd-launch.service \
"

FILES:${PN} = "\
	/conf/rd-launch.sh \
	${systemd_system_unitdir}/rd-launch.service \
"
RDEPENDS:${PN} +="busybox tmux"

do_install() {
	install -d ${D}/conf
	install -m 0755 ${WORKDIR}/rd-launch.sh ${D}/conf/rd-launch.sh
	
	install -d ${D}${systemd_system_unitdir}
	install -m 0644 ${WORKDIR}/rd-launch.service ${D}${systemd_system_unitdir}/rd-launch.service
}

SYSTEMD_SERVICE:${PN} = "rd-launch.service"
