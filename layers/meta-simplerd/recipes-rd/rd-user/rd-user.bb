SUMMARY = "Basic system user"
LICENSE = "GPL-3.0-or-later"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-3.0-or-later;md5=1c76c4cc354acaac30ed4d5eefea7245"

inherit useradd

USERADD_PACKAGES = "${PN}"

USERADD_PARAM:${PN} = "-s /bin/sh -p '\$6\$p31LikuBrReD7/8V\$xfEEb2N3xCzDn.XO0DZeqjuJ.i7fP68Hl6r0Utx8yMxM/4J4mPxDhKUJ25ABuwgbTmqgFnlNnhyWHcx8TWxfI1' rd"

do_install () {
	install -d -m 755 ${D}${datadir}/rd
	touch ${D}${datadir}/rd/file1
	chown -R rd ${D}${datadir}/rd
	chgrp -R rd ${D}${datadir}/rd
}

FILES:${PN} = "${datadir}/rd/*"
