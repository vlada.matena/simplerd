#!/bin/sh

qemu-system-x86_64 -accel kvm -m 4G -smp 2 -hda build/tmp-glibc/deploy/images/intel-corei7-64/simplerd-flash-intel-corei7-64.wic -bios /usr/share/edk2-ovmf/OVMF_CODE.fd -vga std -device e1000,netdev=net0 -netdev tap,id=net0 -display gtk,gl=on,show-cursor=on -vga virtio -device usb-ehci,id=usb,bus=pci.0,addr=0x4 -device usb-tablet $@
